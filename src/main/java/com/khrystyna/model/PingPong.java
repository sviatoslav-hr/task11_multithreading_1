package com.khrystyna.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalTime;

public class PingPong {
    private static Logger logger = LogManager.getLogger(PingPong.class);
    private Thread pingThread;
    private Thread pongThread;
    private boolean pingTurn = true;
    private volatile boolean isFinished = false;
    private String resultMsg;

    public PingPong() {
        pingThread = new Thread(() -> {
            while (!isFinished()) {
                interact();
            }
        }, "Pong!");
        pongThread = new Thread(() -> {
            while (!isFinished()) {
                interact();
            }
        }, "Ping!");
    }

    public void start() {
        PingPong game = new PingPong();

        game.pingThread.start();
        game.pongThread.start();

        LocalTime startTime = LocalTime.now();
        while (!isFinished) {
            if (LocalTime.now().getSecond() == (startTime.plusSeconds(2).getSecond())) {
                resultMsg = "No winner";
                this.isFinished = true;
                logger.debug("isFinished=true____________________________________________________________");
                return;
            }
            if (pingThread.getState() == Thread.State.WAITING
                    && pongThread.getState()
                    == Thread.State.WAITING) {
                notifyAll();
            }
        }
    }

    private synchronized void interact() {
        String name = Thread.currentThread().getName();
        if (pingTurn && name.equals("Ping!")) {
            this.isFinished = true;
            resultMsg = "Ping Thread";
            return;
        } else if (!pingTurn && name.equals("Pong!")) {
            this.isFinished = true;
            resultMsg = "Pong Thread";
            return;
        }

        pingTurn = !pingTurn;
        logger.trace(name);

        logger.debug(pingThread.getState());
        logger.debug(pongThread.getState());

        if (pingThread.getState() == Thread.State.WAITING
                || pongThread.getState()
                == Thread.State.WAITING) {
            logger.debug("notifying...");
            notifyAll();
            logger.debug(pingThread.getState());
            logger.debug(pongThread.getState());
        }

        try {
            logger.debug("waiting...");
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.debug(pingThread.getState());
        logger.debug(pongThread.getState());
    }

    public String getResultMsg() {
        return resultMsg;
    }

    public boolean isFinished() {
        return isFinished;
    }
}
