package com.khrystyna.views.menu;

@FunctionalInterface
public interface Printable {
    void proceed();
}
