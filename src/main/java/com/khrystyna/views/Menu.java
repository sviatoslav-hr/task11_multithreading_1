package com.khrystyna.views;

import com.khrystyna.controllers.Controller;
import com.khrystyna.views.menu.MenuItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Menu implements View {
    private static Logger logger = LogManager.getLogger(Menu.class);
    private Scanner scanner = new Scanner(System.in);
    private Map<Integer, MenuItem> menu = new LinkedHashMap<>();
    private Controller controller = new Controller();
    private boolean exit;

    @Override
    public void start() {
        prepareMenu();

        do {
            displayMenu();
            int index = getInput("Enter number: ");
            MenuItem menuItem = menu.get(index);
            if (menuItem != null) {
                menuItem.getAction().proceed();
            }
        } while (!exit);

    }

    private void prepareMenu() {
        menu.put(1, new MenuItem("Run PingPong Game", this::runPingPong));
//        menu.put(2, new MenuItem("Print tree", controller::printTree));
//        menu.put(3, new MenuItem("Insert element", this::insertElement));
//        menu.put(4, new MenuItem("Search element", this::findElement));
//        menu.put(5, new MenuItem("Remove element", this::removeElement));
//        menu.put(6, new MenuItem("Clear tree", controller::clear));
        menu.put(0, new MenuItem("exit", () -> exit = true));
    }

    private void displayMenu() {
        for (Map.Entry<Integer, MenuItem> entry : menu.entrySet()) {
            logger.trace(entry.getKey() + "\t-\t"
                    + entry.getValue().getDescription());
        }
    }

    private int getInput(String message) {
        System.out.print(message);
        return scanner.nextInt();
    }

    private void runPingPong() {
        controller.startPingPong();

        String winner = controller.getPingPongWinner();
        logger.trace(winner + " won!");
    }
}
