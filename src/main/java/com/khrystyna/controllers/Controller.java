package com.khrystyna.controllers;

import com.khrystyna.model.PingPong;

public class Controller {
    private PingPong pingPong = new PingPong();
    public void startPingPong() {
       pingPong.start();
    }

    public boolean isPingPongFinished() {
        return pingPong.isFinished();
    }

    public String getPingPongWinner() {
        return pingPong.getResultMsg();
    }
}
